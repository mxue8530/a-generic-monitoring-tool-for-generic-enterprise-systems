

import ict.csiro.au.ContextType;
import ict.csiro.au.Event;
import ict.csiro.au.ObjectFactory;
import ict.csiro.au.PropertiesType;
import ict.csiro.au.PropertyType;

import javax.jms.Connection;
import javax.jms.DeliveryMode;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.util.JAXBSource;
import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
 
public class Test
{
	private static String user = ActiveMQConnection.DEFAULT_USER;
	private static String password = ActiveMQConnection.DEFAULT_PASSWORD;
	private static String url = ActiveMQConnection.DEFAULT_BROKER_URL;
	private static String subject = "TOOL.DEFAULT";
    public static void main(String[] args) throws JAXBException, TransformerFactoryConfigurationError, TransformerException
    {
 
        ObjectFactory factory = new ObjectFactory();
 
        ContextType context=factory.createContextType();
  
        PropertyType pType=factory.createPropertyType();
        pType.setKey("4860a189-a390-4fc7-82c5-e6cccfc9cede");
        pType.setValue("441e9d87-5b1a-409f-a0cf-9b485417db3b");
        
        PropertiesType prop=factory.createPropertiesType();
        prop.setGroupName("GroupName");
        
        List<PropertyType> property = new ArrayList<PropertyType>();
        property.add(pType);
        prop.setProperty(property);
        
        List<PropertiesType> pTypes=new ArrayList<PropertiesType>();
        pTypes.add(prop);
        
        context.setProperties(pTypes);
        System.out.println(prop.getProperty());
        
        Event event = factory.createEvent();
        event.setUSERID("Credit Report");
        event.setDATE("2011-02-04 12:15:12");
        event.setLEVEL("szjhiu1-xznba-xniuqn-zqgcxq1-nswavxw2");
        event.setLOGGER("Receiving Message");
        event.setContext(context);
        
        System.out.println(context.getProperties());
        System.out.println(prop.getProperty());
        
        JAXBContext jaxbCountext = JAXBContext.newInstance(Event.class);
        JAXBElement<Event> jaxbElemetn = new JAXBElement(new QName("event"), Event.class, event);
        JAXBSource source = new JAXBSource(jaxbCountext, jaxbElemetn);
        
        StringWriter stringWriter = new StringWriter(); 
        Transformer transformer = TransformerFactory.newInstance().newTransformer(); 
        transformer.transform(source, new StreamResult(stringWriter)); 
        String strFileContent = stringWriter.toString(); 
        System.out.println(strFileContent);
        JDBCExample jdbc=new JDBCExample();
        System.out.println("+++"+event.getDATE()+"+++");
        //jdbc.JDBCExample(strFileContent,event.getDATE(),event.getUSERID());
        sendMessages(strFileContent);
    }
    
    public static void sendMessages(String strLine){
    	Connection connection = null;	 
		try {
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(user, password, url);
			connection = connectionFactory.createConnection();
			connection.start();
			Session session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Destination destination = session.createQueue(subject);
			MessageProducer producer = session.createProducer(destination);
			producer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
			TextMessage message = null;
			message = session.createTextMessage("hello world ");
			message.setText(strLine);
			producer.send(message);
			System.out.println("Message: "+message.getText()+" sent");	
		} catch (JMSException e) {
			e.printStackTrace();
		} 
    }
 
}

