import ict.csiro.au.Event;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
 
import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.bind.util.JAXBSource;
import javax.xml.namespace.QName;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
 
import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQMessage;
import org.apache.activemq.command.ProducerInfo;
 

public class Consumer {
 
	private static String user = ActiveMQConnection.DEFAULT_USER;
	private static String password = ActiveMQConnection.DEFAULT_PASSWORD;
	private String url = ActiveMQConnection.DEFAULT_BROKER_URL;
	private int ackMode = Session.AUTO_ACKNOWLEDGE;
	private String subject = "TOOL.DEFAULT";
	private long receiveTimeOut;
	private static Consumer consumer = null;
	private static Consumer getInstance(){
		if (consumer==null){
			consumer = new Consumer();
		}
		return consumer;
	}
 
	private void receive() throws JMSException, IOException, JAXBException{
			ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(user, password, url);
			Connection connection = connectionFactory.createConnection();
			connection.start();
			Session session = connection.createSession(false, ackMode);
			Destination destination = session.createQueue(subject);
			MessageConsumer consumer = session.createConsumer(destination);
			Message message = consumer.receive(1000);
			if (message instanceof TextMessage) {
				TextMessage txtMsg = (TextMessage) message;
				String msg = txtMsg.getText();
				System.out.println(msg);
				consumer.close();
				session.close();
				connection.close();
				desSync(msg);
			}
			
	}
	
	private void desSync(String str) throws JAXBException{
		
		JAXBContext jaxbCountext = JAXBContext.newInstance(Event.class);
		Unmarshaller unmarshaller = jaxbCountext.createUnmarshaller();
		javax.xml.transform.stream.StreamSource s = new javax.xml.transform.stream.StreamSource(new StringReader(str));
        JAXBElement<ict.csiro.au.Event> e = unmarshaller.unmarshal(s, ict.csiro.au.Event.class );
        System.out.println(e.getValue().getUSERID());
	}
	
	public static void main(String[] args) throws JMSException, IOException, JAXBException{
	        getInstance().receive();
	        EventReader er=new EventReader();
	    }
	
	}
