import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
 
public class JDBCExample {
 
	public void JDBCExample(String str, String datestr, String userid) {
 
		System.out.println("-------- PostgreSQL "
				+ "JDBC Connection Testing ------------");
 
		try {
 
			Class.forName("org.postgresql.Driver");
 
		} catch (ClassNotFoundException e) {
 
			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;
 
		}
 
		System.out.println("PostgreSQL JDBC Driver Registered!");
 
		Connection connection = null;
 
		try {
			
			connection = DriverManager.getConnection(
					"jdbc:postgresql://127.0.0.1:5432/testdb", "postgres",
					"admin");
			String query="INSERT INTO testdb (\"EventName\", \"EventContent\", \"User_Id\", \"EventTime\", \"id\") VALUES (?,?,?,?,?)"; 
			try{
				PreparedStatement ps;
				ps = connection.prepareStatement(query); 
				ps.setString(1, "fax");
				ps.setString(2, str);
				try {
					Date date = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(datestr);
					Calendar calendar=Calendar.getInstance();
					calendar.setTime(date);
					Timestamp ts = new Timestamp(calendar.getTime().getTime());
					System.out.println("***"+ts+"***");
					ps.setTimestamp(4, ts);
				} catch (ParseException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				ps.setInt(5,23);
				ps.setString(3, userid);
				System.out.println(userid);
				ps.executeUpdate();
				
			}
			catch(SQLException ex){
				System.out.println(ex);
			}
 
		} catch (SQLException e) {
 
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;
 
		}
 
		if (connection != null) {
			System.out.println("Database connected!");
		} else {
			System.out.println("Failed to make connection!");
		}
	}
 
}