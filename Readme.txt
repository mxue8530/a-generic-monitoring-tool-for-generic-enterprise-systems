Prerequisites:
	Sun Java SE SDK 1.7 or up
	Eclipse IDE for Java EE Developers
	Apache ActiveMQ  5.8.0
	PostgreSQL 8.3

Setup
1. Install Java SE 1.7, set the enviroment variable.
2. Install PostgreSQL
3. Set PostgreSQL password as 'admin'
4. Create a database table named 'testdb'

Build
1. Open Eclipse
2. Import Java project, XPDLGUI_2.0
3. Import Java project, XSDTest

Start Project
1. Go to /apache_activemq-5.7.0/bin
2. Start activemq.bat
3. Go to the home directory of the project
4. Start GUI.jar
5. Load xpdl or xml file into GUI, if everything is correct, you should see a graph generated according to the loaded file
6. Start Message.jar to send messages

Trouble-shooting
1. Can't find 'java.io.*' in eclipse
Solution: go to eclipse 'Window'->'Preferences'->'Java'-> 'Compiler', set compiler compliance level to 1.7
2. GUI freeze after file loaded
Solution: ActiveMQ is not started






	
