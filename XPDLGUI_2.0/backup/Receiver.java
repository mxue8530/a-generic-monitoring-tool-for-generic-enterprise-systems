import java.awt.Color;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LayoutManager;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.MidpointLocator;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.UpdateManager;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;

public class Receiver{
	private static int a;
	private static String user = ActiveMQConnection.DEFAULT_USER;
	private static String password = ActiveMQConnection.DEFAULT_PASSWORD;
	private static String url = ActiveMQConnection.DEFAULT_BROKER_URL;
	private static int ackMode = Session.AUTO_ACKNOWLEDGE;
	private static String subject = "TOOL.DEFAULT";
	private long receiveTimeOut;
    GraphNode node1 = null,node2 = null;
    
	public void receive(final List <Node_info> node_info,final Graph graph) throws JMSException{
		class ProgressBarRunning extends Thread{
			public void run() {
				for (a = 0; a <10; a++) {
					try {
						Thread.sleep(500);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				Display.getDefault().asyncExec(new Runnable(){
					public void run(){
						GraphConnection gc=new GraphConnection(graph, SWT.NONE, node1, node2);
						if(a%2==1)
							gc.highlight();
						else
							gc.unhighlight();
					}
			});
			}
		}
		}
		
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(user, password, url);
		Connection connection = connectionFactory.createConnection();
		connection.start();
		Session session = connection.createSession(false, ackMode);
		Destination destination = session.createQueue(subject);
		final MessageConsumer consumer = session.createConsumer(destination);
		consumer.setMessageListener(new MessageListener(){
			@Override
			public void onMessage(Message m) {
				// TODO Auto-generated method stub
				TextMessage textMsg = (TextMessage) m;
				 try {	
					 	System.out.println(textMsg.getText());
	                    String sender=textMsg.getStringProperty("Sender");
	                    String receiver=textMsg.getStringProperty("Receiver");
					 for(int i=0;i<node_info.size();i++){
						 if(node_info.get(i).activity.getId().equals(sender)){
							 node1=node_info.get(i).gnode;
						 }
						 else if(node_info.get(i).activity.getId().equals(receiver)){
							 node2=node_info.get(i).gnode;
						 }
					 }
					 new ProgressBarRunning().start(); 
	                } catch (JMSException e) {
	                    e.printStackTrace();
	                }
			}			
		});			
}
	
}