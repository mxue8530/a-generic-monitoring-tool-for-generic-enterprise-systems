import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.jms.JMSException;

import org.eclipse.nebula.widgets.cdatetime.CDT;
import org.eclipse.nebula.widgets.cdatetime.CDateTime;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Menu;
import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.FileDialog;
import org.eclipse.swt.widgets.MenuItem;
import org.eclipse.swt.widgets.CoolBar;
import org.eclipse.swt.widgets.CoolItem;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.swt.widgets.ToolBar;
import org.eclipse.swt.widgets.ToolItem;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.layout.FillLayout;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.RowLayout;
import org.eclipse.swt.layout.FormLayout;
import org.eclipse.swt.layout.FormData;
import org.eclipse.swt.layout.FormAttachment;
import org.eclipse.swt.layout.RowData;
import org.eclipse.swt.custom.CTabFolder;
import org.eclipse.swt.custom.CTabFolder2Adapter;
import org.eclipse.swt.custom.CTabFolderEvent;
import org.eclipse.swt.custom.CTabItem;
import org.eclipse.swt.custom.ScrolledComposite;
import org.eclipse.swt.custom.StackLayout;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.custom.SashForm;
import org.eclipse.swt.graphics.Point;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.custom.CBanner;
import org.eclipse.swt.widgets.TabFolder;
import org.eclipse.swt.widgets.TabItem;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.widgets.Text;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.layouts.LayoutStyles;
import org.eclipse.zest.layouts.algorithms.DirectedGraphLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.GridLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.RadialLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.SpringLayoutAlgorithm;
import org.eclipse.zest.layouts.algorithms.TreeLayoutAlgorithm;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableColumn;

public class XPDLGUI {

	protected Shell shell;
	List<Graph> graphList=new ArrayList(); 
	List<CTabItem> tabItemList=new ArrayList();
	DataRetrieval jdbc=new DataRetrieval();
	private Text text;
	Tree tree;
	private Table table;
	/**
	 * Launch the application.
	 * @param args
	 */
	public static void main(String[] args) {
		try {
			XPDLGUI window = new XPDLGUI();
			window.open();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setToolTipText("");
		shell.setImage(SWTResourceManager.getImage(XPDLGUI.class, "/resource/CSIRO_Grad_RGB_hr.png"));
		shell.setSize(449, 304);
		shell.setText("CSIRO Distributed Systems Monitoring Tool");
		shell.setLayout(new FormLayout());
		//shell.setLayout(null);
		
		
		Menu menu = new Menu(shell, SWT.BAR);
		shell.setMenuBar(menu);
		
		MenuItem mntmNewSubmenu = new MenuItem(menu, SWT.CASCADE);
		mntmNewSubmenu.setText("File");
		
		Menu menu_1 = new Menu(mntmNewSubmenu);
		mntmNewSubmenu.setMenu(menu_1);
		
		MenuItem mntmNewItem = new MenuItem(menu_1, SWT.NONE);
		mntmNewItem.setText("New Item");
		
		MenuItem mntmNewItem_1 = new MenuItem(menu_1, SWT.NONE);
		mntmNewItem_1.setText("New Item");
		
		MenuItem mntmNewSubmenu_1 = new MenuItem(menu, SWT.CASCADE);
		mntmNewSubmenu_1.setText("Tool");
		
		Menu menu_2 = new Menu(mntmNewSubmenu_1);
		mntmNewSubmenu_1.setMenu(menu_2);
		
		MenuItem mntmNewSubmenu_2 = new MenuItem(menu_2, SWT.CASCADE);
		mntmNewSubmenu_2.setText("Layout");
		
		Menu menu_3 = new Menu(mntmNewSubmenu_2);
		mntmNewSubmenu_2.setMenu(menu_3);	
		
		MenuItem mntmNewItem_3 = new MenuItem(menu_2, SWT.NONE);
		mntmNewItem_3.setText("New Item");
		
		CoolBar coolBar = new CoolBar(shell, SWT.FLAT);

		FormData fd_coolBar = new FormData();
		fd_coolBar.top = new FormAttachment(0, 5);
		fd_coolBar.left = new FormAttachment(0, 10);
		coolBar.setLayoutData(fd_coolBar);
		//cool item
		CoolItem coolItem = new CoolItem(coolBar, SWT.NONE);
		
		ToolBar toolBar = new ToolBar(coolBar, SWT.FLAT | SWT.RIGHT);
		
		ToolItem Open = new ToolItem(toolBar, SWT.NONE);
		Open.setToolTipText("Open");
		Open.setImage(SWTResourceManager.getImage(XPDLGUI.class, "/resource/open.png"));
		toolBar.pack();
		ToolItem Save = new ToolItem(toolBar, SWT.NONE);
		Save.setToolTipText("Save");
		Save.setImage(SWTResourceManager.getImage(XPDLGUI.class, "/resource/save.png"));
		
		Point size = toolBar.getSize();
		coolItem.setControl(toolBar);
		coolItem.setSize(new Point(133, 54));

		CoolItem coolItem_1 = new CoolItem(coolBar, SWT.NONE);
		
		ToolBar toolBar_1 = new ToolBar(coolBar, SWT.FLAT | SWT.RIGHT);
		coolItem_1.setControl(toolBar_1);
		
		ToolItem Back = new ToolItem(toolBar_1, SWT.NONE);
		Back.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				jdbc.redo();
			}
		});
		Back.setToolTipText("Step Back");
		Back.setImage(SWTResourceManager.getImage(XPDLGUI.class, "/resource/back.png"));
		 
		ToolItem Next = new ToolItem(toolBar_1, SWT.NONE);
		Next.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				jdbc.nextStep();
			}
		});
		Next.setToolTipText("Next Step");
		Next.setImage(SWTResourceManager.getImage(XPDLGUI.class, "/resource/next.png"));
		toolBar_1.pack();
		size = toolBar_1.getSize();
		coolItem_1.setControl(toolBar_1);
		coolItem_1.setSize(new Point(133, 54));
		
		CoolItem coolItem_2 = new CoolItem(coolBar, SWT.NONE);
		
		ToolBar toolBar_2 = new ToolBar(coolBar, SWT.FLAT | SWT.RIGHT);
		coolItem_2.setControl(toolBar_2);
		
		ToolItem LoadEvents = new ToolItem(toolBar_2, SWT.NONE);
		LoadEvents.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				final Shell dialog = new Shell(shell,SWT.DIALOG_TRIM);
				dialog.setMinimumSize(new Point(250, 150));
				dialog.setText("Time Machine");
				dialog.setSize(232, 94);
				dialog.setLayout(null);
				final CDateTime start = new CDateTime(dialog, CDT.BORDER | CDT.COMPACT | CDT.DROP_DOWN 
						| CDT.DATE_MEDIUM | CDT.TIME_MEDIUM);
				start.setBounds(55, 10, 179, 23);
				start.setPattern("yyyy-MM-dd HH:mm:ss");
				final CDateTime end = new CDateTime(dialog, CDT.BORDER | CDT.COMPACT | CDT.DROP_DOWN 
						| CDT.DATE_MEDIUM | CDT.TIME_MEDIUM);
				end.setBounds(55, 38, 179, 23);
				end.setPattern("yyyy-MM-dd HH:mm:ss");
				dialog.pack();
				
				Label lblStart = new Label(dialog, SWT.NONE);
				lblStart.setBounds(14, 10, 35, 15);
				lblStart.setText("Start");
				
				Label lblEnd = new Label(dialog, SWT.NONE);
				lblEnd.setBounds(14, 46, 35, 15);
				lblEnd.setText("End");
				
				Button btnOk = new Button(dialog, SWT.NONE);
				btnOk.addSelectionListener(new SelectionAdapter() {
					@Override
					public void widgetSelected(SelectionEvent e) {
						String query="SELECT * FROM testdb WHERE \"EventTime\" BETWEEN "  +"'"+start.getText()+"'"+ "AND "+"'"+end.getText()+"'"+"ORDER BY \"EventTime\"";;
						System.out.println(start.getText());
						System.out.println(end.getText());
						jdbc.dbConnection(query);
						dialog.dispose();
					}
				});
				btnOk.setBounds(159, 87, 75, 25);
				btnOk.setText("OK");
				dialog.open();
			}
		});
		LoadEvents.setImage(SWTResourceManager.getImage(XPDLGUI.class, "/resource/database.png"));
		LoadEvents.setToolTipText("Load Events");
		
		ToolItem tltmNewItem_5 = new ToolItem(toolBar_2, SWT.NONE);
		tltmNewItem_5.setImage(SWTResourceManager.getImage(XPDLGUI.class, "/resource/tool.png"));
		tltmNewItem_5.setToolTipText("Tool");
		toolBar_2.pack();
		size = toolBar_2.getSize();
		coolItem_2.setControl(toolBar_2);
		coolItem_2.setSize(coolItem_2.computeSize(size.x, size.y));
		
		SashForm sashForm = new SashForm(shell, SWT.NONE);
		FormData fd_sashForm = new FormData();
		fd_sashForm.top = new FormAttachment(coolBar, 6);
		fd_sashForm.left = new FormAttachment(0, 10);
		fd_sashForm.right = new FormAttachment(100, -21);
		sashForm.setLayoutData(fd_sashForm);
		
		final CTabFolder tabFolder = new CTabFolder(sashForm, SWT.BORDER|SWT.CLOSE);
		tabFolder.setSelectionBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT));   
        tabFolder.setSimple(false); 
      
		CTabItem tbtmNewItem = new CTabItem(tabFolder, SWT.CLOSE);
		tbtmNewItem.setText("New Item");
		tabFolder.setSelection(tbtmNewItem);
		Composite composite_2 = new Composite(tabFolder, SWT.NONE);
		tbtmNewItem.setControl(composite_2);
		tree=new Tree(composite_2,SWT.BORDER);
		tree.setLocation(0, 0);
		composite_2.setLayout(new FillLayout());

		System.out.println("size "+composite_2.getSize());
		SashForm sashForm_1 = new SashForm(sashForm, SWT.VERTICAL);
		
		final CTabFolder tabFolder_1 = new CTabFolder(sashForm_1, SWT.BORDER|SWT.CLOSE);
		tabFolder_1.setSelectionBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT));
		tabFolder_1.setSimple(false);
		
		CTabFolder tabFolder_2 = new CTabFolder(sashForm_1, SWT.BORDER);
		tabFolder_2.setSelectionBackground(Display.getCurrent().getSystemColor(SWT.COLOR_TITLE_INACTIVE_BACKGROUND_GRADIENT));
		
		CTabItem tbtmNewItem_1 = new CTabItem(tabFolder_2, SWT.NONE);
		tbtmNewItem_1.setText("Output");
		
		final StyledText styledText = new StyledText(tabFolder_2, SWT.BORDER|SWT.H_SCROLL|SWT.V_SCROLL);
		tbtmNewItem_1.setControl(styledText);
		
		CTabItem tbtmNewItem_2 = new CTabItem(tabFolder_2, SWT.NONE);
		tbtmNewItem_2.setText("Messages");
		
		table = new Table(tabFolder_2, SWT.BORDER | SWT.FULL_SELECTION);
		tbtmNewItem_2.setControl(table);
		table.setHeaderVisible(true);
		table.setLinesVisible(true);
		
		TableColumn tblclmnNewColumn = new TableColumn(table, SWT.NONE);
		tblclmnNewColumn.setWidth(100);
		tblclmnNewColumn.setText("Name");
		
		TableColumn tblclmnNewColumn_1 = new TableColumn(table, SWT.NONE);
		tblclmnNewColumn_1.setWidth(100);
		tblclmnNewColumn_1.setText("Id");
		
		TableColumn tblclmnNewColumn_2 = new TableColumn(table, SWT.NONE);
		tblclmnNewColumn_2.setWidth(100);
		tblclmnNewColumn_2.setText("Time");
		
		TableColumn tblclmnNewColumn_3 = new TableColumn(table, SWT.NONE);
		tblclmnNewColumn_3.setWidth(100);
		tblclmnNewColumn_3.setText("Message");
		sashForm_1.setWeights(new int[] {2, 2});
		sashForm.setWeights(new int[] {1, 2});
		
		Composite composite = new Composite(shell, SWT.NONE);
		fd_sashForm.bottom = new FormAttachment(100, -28);
		FormData fd_composite = new FormData();
		fd_composite.top = new FormAttachment(sashForm, 6);
		fd_composite.left = new FormAttachment(0, 5);
		fd_composite.bottom = new FormAttachment(100, -5);
		fd_composite.right = new FormAttachment(0, 407);
		composite.setLayoutData(fd_composite);
		
		text = new Text(composite, SWT.BORDER);
		text.setBounds(0, 0, 76, 21);
		
		Label Logo = new Label(shell, SWT.NONE);
		fd_coolBar.right = new FormAttachment(Logo, -6);
		Logo.setImage(SWTResourceManager.getImage(XPDLGUI.class, "/resource/CSIRO_Grad_RGB_hr.png"));
		FormData fd_Logo = new FormData();
		fd_Logo.top = new FormAttachment(0, 5);
		fd_Logo.right = new FormAttachment(100, -10);
		fd_Logo.left = new FormAttachment(100, -64);
		Logo.setLayoutData(fd_Logo);
		Open.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				FileDialog fd = new FileDialog(shell, SWT.MULTI);
				XPDLReader reader= new XPDLReader();
				Composite composite_1 = new Composite(tabFolder_1,SWT.NONE);
				composite_1.setLayout(new FillLayout());
				CTabItem tabItem = new CTabItem(tabFolder_1, SWT.CLOSE);
				tabFolder_1.setSelection(tabItem);
				tabItem.setControl(composite_1);
				fd.setFilterExtensions(new String[]{"*.xml","*.xpdl"});
				if ( fd.open()!= null) {
					String[] filenames = fd.getFileNames();
					for (String filename : filenames) {
						File f = new File(fd.getFilterPath() + File.separator+ filename);
						tabItem.setText(filename);
						try {
							graphList.add(reader.drawing(composite_1,styledText,table));
							tabItemList.add(tabItem);
						} catch (JMSException | IOException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						reader.xml_to_obj(f,tree,table);	
						
					}
				}
			}
		});
		Save.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			}
		});
		MenuItem mntmTreeLayout = new MenuItem(menu_3, SWT.RADIO);
		mntmTreeLayout.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				for(int i=0;i<tabItemList.size();i++){
					if(tabItemList.get(i).equals(tabFolder_1.getSelection())){
						graphList.get(i).setLayoutAlgorithm(new TreeLayoutAlgorithm( 
						LayoutStyles.NO_LAYOUT_NODE_RESIZING), true);
					}
				}
				
			}
		});
		mntmTreeLayout.setText("Tree Layout");
		MenuItem mntmNewCheckbox = new MenuItem(menu_3, SWT.RADIO);
		mntmNewCheckbox.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				for(int i=0;i<tabItemList.size();i++){
					if(tabItemList.get(i).equals(tabFolder_1.getSelection()))
						graphList.get(i).setLayoutAlgorithm(new SpringLayoutAlgorithm( 
								LayoutStyles.NO_LAYOUT_NODE_RESIZING), true);
				}
			}
		});
		mntmNewCheckbox.setText("Spring Layout");
		
		MenuItem mntmNewCheckbox_1 = new MenuItem(menu_3, SWT.RADIO);
		mntmNewCheckbox_1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				for(int i=0;i<tabItemList.size();i++){
					if(tabItemList.get(i).equals(tabFolder_1.getSelection()))
						graphList.get(i).setLayoutAlgorithm(new RadialLayoutAlgorithm( 
								LayoutStyles.NO_LAYOUT_NODE_RESIZING), true); 
				}
			}
		});
		mntmNewCheckbox_1.setText("Radial Layout");
		
		MenuItem mntmNewCheckbox_2 = new MenuItem(menu_3, SWT.RADIO);
		mntmNewCheckbox_2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				for(int i=0;i<tabItemList.size();i++){
					if(tabItemList.get(i).equals(tabFolder_1.getSelection()))
						graphList.get(i).setLayoutAlgorithm(new DirectedGraphLayoutAlgorithm( 
								LayoutStyles.NO_LAYOUT_NODE_RESIZING), true);
				}
			}
		});
		mntmNewCheckbox_2.setText("Directed Graph Layout");
		
		MenuItem mntmNewCheckbox_3 = new MenuItem(menu_3, SWT.RADIO);
		mntmNewCheckbox_3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent arg0) {
				for(int i=0;i<tabItemList.size();i++){
					if(tabItemList.get(i).equals(tabFolder_1.getSelection()))
						graphList.get(i).setLayoutAlgorithm(new GridLayoutAlgorithm(
								LayoutStyles.NO_LAYOUT_NODE_RESIZING), true);
				}
			}
		});
		mntmNewCheckbox_3.setText("Grid Layout");
		
		MenuItem mntmEdit = new MenuItem(menu, SWT.CASCADE);
		mntmEdit.setText("Edit");
		
		Menu menu_4 = new Menu(mntmEdit);
		mntmEdit.setMenu(menu_4);
		
		MenuItem menuItem_1 = new MenuItem(menu_4, SWT.NONE);
		menuItem_1.setText("New Item");
		
		MenuItem menuItem_2 = new MenuItem(menu_4, SWT.NONE);
		menuItem_2.setText("New Item");
		
		MenuItem mntmView = new MenuItem(menu, SWT.CASCADE);
		mntmView.setText("View");
		
		Menu menu_5 = new Menu(mntmView);
		mntmView.setMenu(menu_5);
		
		MenuItem menuItem_3 = new MenuItem(menu_5, SWT.NONE);
		menuItem_3.setText("New Item");
		
		MenuItem menuItem_4 = new MenuItem(menu_5, SWT.NONE);
		menuItem_4.setText("New Item");
		
		MenuItem mntmHelp = new MenuItem(menu, SWT.CASCADE);
		mntmHelp.setText("Help");
		
		Menu menu_6 = new Menu(mntmHelp);
		mntmHelp.setMenu(menu_6);
		
		MenuItem menuItem_5 = new MenuItem(menu_6, SWT.NONE);
		menuItem_5.setText("New Item");
		
		MenuItem menuItem_6 = new MenuItem(menu_6, SWT.NONE);
		menuItem_6.setText("New Item");
	}
}
