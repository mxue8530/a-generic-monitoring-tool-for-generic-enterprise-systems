import org.eclipse.swt.graphics.Image;
import org.eclipse.wb.swt.SWTResourceManager;

class SetImages{
	public Image SetImage(org.wfmc._2008.xpdl2.Activity x){
		Image image = null;
		switch(x.getName()){
			case "Start":
				image=SWTResourceManager.getImage(XPDLGUI.class, "/resource/loan/documents.png");
				break;
			case "Submission":
				image=SWTResourceManager.getImage(XPDLGUI.class, "/resource/loan/fax.png");
				break;
			case "Title Search":
				image=SWTResourceManager.getImage(XPDLGUI.class, "/resource/loan/search.png");
				break;
			case "Credit Report":
				image=SWTResourceManager.getImage(XPDLGUI.class, "/resource/loan/credit-cards.png");
				break;
			case "Schedule Appraisal":
				image=SWTResourceManager.getImage(XPDLGUI.class, "/resource/loan/check.png");
				break;	
			case "Appraisal Report":
				image=SWTResourceManager.getImage(XPDLGUI.class, "/resource/loan/checklist.png");
				break;	
			case "Application Approval":
				image=SWTResourceManager.getImage(XPDLGUI.class, "/resource/loan/checklist.png");
				break;	
			case "Reject":
				image=SWTResourceManager.getImage(XPDLGUI.class, "/resource/loan/reject.png");
				break;
			case "Accept":
				image=SWTResourceManager.getImage(XPDLGUI.class, "/resource/loan/money.png");
				break;
			case "General Closing Documents":
				image=SWTResourceManager.getImage(XPDLGUI.class, "/resource/loan/documents.png");
				break;
		}
		return image;
	}
}