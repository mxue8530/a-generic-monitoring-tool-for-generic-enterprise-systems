import org.eclipse.swt.SWT;
import org.eclipse.swt.widgets.Display;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;

public class Animations implements Runnable{
	private static int a;
	Graph graph;
	GraphNode sender, receiver;
	public void setElements(final Graph graph,final GraphNode sender,final GraphNode receiver){
		this.graph=graph;
		this.sender=sender;
		this.receiver=receiver;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		for (a = 0; a <10; a++) {
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		Display.getDefault().asyncExec(new Runnable(){
			public void run(){
				GraphConnection gc=new GraphConnection(graph, SWT.NONE, sender, receiver);
				if(a%2==1)
					gc.highlight();
				else
					gc.unhighlight();
			}
	});
	}
	}
}