import ict.csiro.au.Event;

import java.awt.Color;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Timer;

import javax.jms.Connection;
import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.eclipse.draw2d.ChopboxAnchor;
import org.eclipse.draw2d.ColorConstants;
import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.draw2d.LayoutManager;
import org.eclipse.draw2d.LineBorder;
import org.eclipse.draw2d.MidpointLocator;
import org.eclipse.draw2d.Panel;
import org.eclipse.draw2d.PolygonDecoration;
import org.eclipse.draw2d.PolylineConnection;
import org.eclipse.draw2d.RectangleFigure;
import org.eclipse.draw2d.UpdateManager;
import org.eclipse.draw2d.geometry.Rectangle;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;

public class Receiver{
	private static int a;
	private static String user = ActiveMQConnection.DEFAULT_USER;
	private static String password = ActiveMQConnection.DEFAULT_PASSWORD;
	private static String url = ActiveMQConnection.DEFAULT_BROKER_URL;
	private static int ackMode = Session.AUTO_ACKNOWLEDGE;
	private static String subject = "TOOL.DEFAULT";
	private long receiveTimeOut;
    GraphNode node1 = null,node2 = null;
    List <Node_info> nodes=null;
    Table table;
    StyledText styledText;
	public void receive(final List <Node_info> node_info,final Graph graph, final StyledText styledText, final Table table) throws JMSException{
		this.table=table;
		nodes=node_info;
		ActiveMQConnectionFactory connectionFactory = new ActiveMQConnectionFactory(user, password, url);
		final Connection connection = connectionFactory.createConnection();
		connection.start();
		final Session session = connection.createSession(false, ackMode);
		Destination destination = session.createQueue(subject);
		final MessageConsumer consumer = session.createConsumer(destination);
		consumer.setMessageListener(new MessageListener(){
			@Override
			public void onMessage(Message m) {
				// TODO Auto-generated method stub
				if (m instanceof TextMessage) {
					TextMessage txtMsg = (TextMessage) m;
					String msg = null;
					try {
						msg = txtMsg.getText();
					} catch (JMSException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					System.out.println(msg);
					try {
						desSync(msg);
						Animations an = new Animations();
						an.setElements(graph, node1, node2);
						//an.run();
					} catch (JAXBException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}			
		});			
}
	private void desSync(String str) throws JAXBException{
		JAXBContext jaxbCountext = JAXBContext.newInstance(Event.class);
		Unmarshaller unmarshaller = jaxbCountext.createUnmarshaller();
		javax.xml.transform.stream.StreamSource s = new javax.xml.transform.stream.StreamSource(new StringReader(str));
        JAXBElement<ict.csiro.au.Event> e = unmarshaller.unmarshal(s, ict.csiro.au.Event.class );
        EventReader er = new EventReader();
        er.xml_to_obj(e);
        DataStorage ds=new DataStorage();
        ds.JDBCExample(str,e.getValue().getDATE(),e.getValue().getUSERID());
        System.out.println(er.startNodeId);
        if(e.getValue()!=null&&er.getStartNodeId()!=null){
        	 System.out.println("thread "+er.startNodeId);
        	FillTable ft=new FillTable();
        	ft.setElements(table, e.getValue(),er.getStartNodeId());
        	ft.run();
        }
        for(int i=0;i<nodes.size();i++){
        	if(nodes.get(i).activity.getId().equals(er.getStartNodeId()))
        		node1=nodes.get(i).gnode;
        	else if(nodes.get(i).activity.getId().equals(er.getEndNodeId()))
        		node2=nodes.get(i).gnode;
        }
	}
}