import java.sql.DriverManager;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
 
public class DataRetrieval {
	static Queue<EventData> eventQueue = new LinkedList<EventData>();
	static Stack<EventData> eventStack=new Stack<EventData>();	
	public static void dbConnection(String query){
		System.out.println("-------- PostgreSQL "
				+ "JDBC Connection Testing ------------");
		try {
			Class.forName("org.postgresql.Driver");
		} catch (ClassNotFoundException e) { 
			System.out.println("Where is your PostgreSQL JDBC Driver? "
					+ "Include in your library path!");
			e.printStackTrace();
			return;
		} 
		System.out.println("PostgreSQL JDBC Driver Registered!");
		Connection connection = null;
		try {
			connection = DriverManager.getConnection(
					"jdbc:postgresql://127.0.0.1:5432/testdb", "postgres",
					"admin");
			Statement st=connection.createStatement();
			ResultSet rs = st.executeQuery(query); 
			while(rs.next()){
				EventData ed = new EventData();
				ed.EventName=rs.getString(1);
				ed.EventContent=rs.getString(2);
				ed.User_Id=rs.getString(3);
				ed.date=rs.getTimestamp(4);
				ed.id=rs.getString(5);
				eventQueue.add(ed);
				System.out.println(ed.date);
			}
		} catch (SQLException e) { 
			System.out.println("Connection Failed! Check output console");
			e.printStackTrace();
			return;
		}
		if (connection != null) {
			System.out.println("You made it, take control your database now!");
		} else {
			System.out.println("Failed to make connection!");
		}
	}
	public static void nextStep(){
		EventData temp=new EventData();
		temp=eventQueue.poll();
		eventStack.push(temp);
		System.out.println(temp.date);
	}
	public static void redo(){
		EventData temp=new EventData();
		temp=eventStack.pop();
		Queue<EventData> tempQueue = new LinkedList<EventData>();
		tempQueue.add(temp);
		tempQueue.addAll(eventQueue);
		eventQueue=tempQueue;
		System.out.println(temp.date);
	}
}