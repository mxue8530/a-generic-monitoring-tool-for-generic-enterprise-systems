import java.io.File;
import java.io.FileInputStream;
import java.util.Iterator;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.Unmarshaller;

import org.eclipse.zest.core.widgets.GraphNode;

import ict.csiro.au.ContextType;
import ict.csiro.au.Event;
import ict.csiro.au.ObjectFactory;
import ict.csiro.au.PropertiesType;
import ict.csiro.au.PropertyType;

public class EventReader{
	final static String AppPackageName = "ict.csiro.au";
	String startNodeId,endNodeId;
	 public void xml_to_obj(JAXBElement<Event> e)
	 {          
			Event event = e.getValue();
			ict.csiro.au.ContextType cp=event.getContext();
			List<ict.csiro.au.PropertiesType> ptl=cp.getProperties();				
			for( Iterator i = ptl.iterator(); i.hasNext(); ) {
				ict.csiro.au.PropertiesType pt=(PropertiesType) i.next();
				List<ict.csiro.au.PropertyType> prl=pt.getProperty();
				for(Iterator j = prl.iterator(); j.hasNext(); ){
					ict.csiro.au.PropertyType prt=(PropertyType) j.next();
					System.out.println("key "+prt.getKey());
					startNodeId=prt.getKey();
					System.out.println("value "+prt.getValue());
					endNodeId=prt.getValue();
				}
			}
	 }
	 public String getStartNodeId(){
		return startNodeId;	 
	 }
	 public String getEndNodeId(){
		 return endNodeId;
	 }
}