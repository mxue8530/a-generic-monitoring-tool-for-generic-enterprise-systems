import org.eclipse.draw2d.IFigure;
import org.eclipse.draw2d.Label;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.events.MouseEvent;
import org.eclipse.swt.events.MouseListener;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.graphics.Font;
import org.eclipse.swt.graphics.Image;
import org.eclipse.swt.widgets.Canvas;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Listener;
import org.eclipse.swt.widgets.Shell;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.Tree;
import org.eclipse.swt.widgets.TreeItem;
import org.eclipse.wb.swt.SWTResourceManager;
import org.eclipse.zest.core.viewers.internal.GraphModelFactory;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;
import org.eclipse.zest.core.widgets.ZestStyles;
import org.eclipse.zest.layouts.LayoutStyles;
import org.eclipse.zest.layouts.algorithms.*;
import org.wfmc._2002.xpdl1.*;
import org.wfmc._2008.xpdl2.*;
import java.io.*;
import java.util.*;

import javax.jms.JMSException;
import javax.xml.bind.*;

public class XPDLReader
{   
	static Graph graph;
    final static String AppPackageName = "org.wfmc._2008.xpdl2";
    static List<Node_info> node_info=new ArrayList<>(); 
    Receiver receiver=new Receiver();
    static Tree tree;
    static TreeItem root;
    static Composite composite1;
    StyledText styledText;
    Table table;
    static public void print_usage()
    {
        System.out.println("Usage: java Tester action xmlFileName");
        System.out.println("where: action = -s | -d");
        System.exit(0);
    }

    public void xml_to_obj(File file, Tree tree2,  Table table)
    {
    	tree=tree2;
    	root= new TreeItem(tree, 0);
		root.setText(file.getName());
		System.out.println("\nxml_to_obj() called...");
		try
		{          
            System.out.println("To create JAXBContext.newInstance");
            JAXBContext jc = JAXBContext.newInstance(AppPackageName);
            System.out.println("To create an Unmarshaller...");
            Unmarshaller u = jc.createUnmarshaller();
            System.out.println();
			javax.xml.transform.stream.StreamSource s = new javax.xml.transform.stream.StreamSource(new FileInputStream(file));
            System.out.println("To instance an object from a xml file: " + file.getName());
			JAXBElement<org.wfmc._2008.xpdl2.PackageType> e = u.unmarshal(s, org.wfmc._2008.xpdl2.PackageType.class );
			org.wfmc._2008.xpdl2.PackageType p = e.getValue();
            		print(p);
		}
      		catch(Exception e ) 
		{
            		e.printStackTrace();
    		}
	}

    public static void print(org.wfmc._2008.xpdl2.PackageType x) 
    {
    	System.out.println("to print the xpdl package:");
    	org.wfmc._2008.xpdl2.WorkflowProcesses y = x.getWorkflowProcesses();
    	print(y);
    }

    public static void print(org.wfmc._2008.xpdl2.WorkflowProcesses x) 
    {
    	System.out.println("to print a WorkflowProcesses:");
        List<org.wfmc._2008.xpdl2.ProcessType> l = x.getWorkflowProcess();
        // iterate over List
        for( Iterator i = l.iterator(); i.hasNext(); ) 
        {
        	org.wfmc._2008.xpdl2.ProcessType y = (org.wfmc._2008.xpdl2.ProcessType) i.next(); 
        	print(y); 
        }
    }

    public static void print(org.wfmc._2008.xpdl2.ProcessType x) 
    {
    	System.out.println("to print the process:");
        System.out.println( "workflowProcess.id   = " + x.getId() ); 
        System.out.println( "workflowProcess.name = " + x.getName() );
        List<java.lang.Object> l = x.getContent();
        TreeItem TreeProcess=new TreeItem(root,0);
        TreeProcess.setText(x.getName());
        TreeProcess.setData(x.getId());
        // iterate over List
        for( Iterator i = l.iterator(); i.hasNext(); ) 
	  {
		try{
            org.wfmc._2008.xpdl2.Activities y = (org.wfmc._2008.xpdl2.Activities) i.next();
			print(y,TreeProcess);
			org.wfmc._2008.xpdl2.Transitions t = (org.wfmc._2008.xpdl2.Transitions) i.next();
			print(t);
		}
		catch(Exception e){} 
	  }
    }

    
    public static void print(org.wfmc._2008.xpdl2.Transitions x) 
    {
    	System.out.println("to print a Transitions:");
        List<org.wfmc._2008.xpdl2.Transition> l = x.getTransition();
        for( Iterator i = l.iterator(); i.hasNext(); ) 
        {
            org.wfmc._2008.xpdl2.Transition y = (org.wfmc._2008.xpdl2.Transition) i.next();  
            print(y); 
        }
    }
    
    public static void print(org.wfmc._2008.xpdl2.Transition x){
    	GraphNode node1 = null,node2 = null;
    	for(int i=0;i<node_info.size();i++){
    		if(node_info.get(i).activity.getId().equals(x.getFrom()))
    			node1=node_info.get(i).gnode;
    		else if(node_info.get(i).activity.getId().equals(x.getTo()))
    			node2=node_info.get(i).gnode;
    	}
    		new GraphConnection(graph, SWT.NONE, node1, node2);
    }
    
    public static void print(org.wfmc._2008.xpdl2.Activities x,TreeItem TreeProcess) 
    {
    	System.out.println("to print a Activities:");
        List<org.wfmc._2008.xpdl2.Activity> l = x.getActivity();
        for( Iterator i = l.iterator(); i.hasNext(); ) 
        {
            org.wfmc._2008.xpdl2.Activity y = (org.wfmc._2008.xpdl2.Activity) i.next();
            print(y,TreeProcess); 
        }
    }

    public static void print(org.wfmc._2008.xpdl2.Activity x,TreeItem TreeProcess) 
    {
    	System.out.println("to print a Activity:");
    	System.out.println( "Activity.id   = " + x.getId() ); 
    	System.out.println( "Activity.name = " + x.getName() );
    	TreeItem TreeActivity=new TreeItem(TreeProcess,0);
    	if(x.getName()!=null){
        	TreeActivity.setText(x.getName());
        	TreeActivity.setData(x.getId());
        }
        else{
        	TreeActivity.setText(x.getId());
        	TreeActivity.setData(x);
        }
        List<java.lang.Object> l = x.getContent();
        Node_info ni=new Node_info();
        ni.activity=x;
        node_info.add(ni);    
    	GraphNode gnode=new GraphNode(graph, ZestStyles.NODES_NO_LAYOUT_RESIZE, ni.activity.getName());
    	gnode.setFont(new Font(null, "Arial", 12, SWT.BOLD ) );
    	SetImages setImage=new SetImages();
    	gnode.setImage(setImage.SetImage(x));
    	ni.gnode=gnode;
        for(Iterator i=l.iterator();i.hasNext();){
        	try {
        		org.wfmc._2008.xpdl2.NodeGraphicsInfos y = (org.wfmc._2008.xpdl2.NodeGraphicsInfos) i.next();        		
        		print(y,gnode);
        	}
        	catch(Exception e){} 
        }
        
    }

    public static void print(org.wfmc._2008.xpdl2.NodeGraphicsInfos x, GraphNode gnode) 
    {
        List<org.wfmc._2008.xpdl2.NodeGraphicsInfo> l = x.getNodeGraphicsInfo();
        for( Iterator i = l.iterator(); i.hasNext(); ) 
        {
            org.wfmc._2008.xpdl2.NodeGraphicsInfo y = (org.wfmc._2008.xpdl2.NodeGraphicsInfo) i.next();
            print(y,gnode); 
        }
    }

    public static void print(org.wfmc._2008.xpdl2.NodeGraphicsInfo x,GraphNode gnode) 
    {
        org.wfmc._2008.xpdl2.Coordinates c = x.getCoordinates();
    	int Xcoord = c.getXCoordinate().intValue();
    	int Ycoord = c.getYCoordinate().intValue();
    	gnode.setLocation(Xcoord,Ycoord);
    	composite1.setSize(composite1.getSize().x+1,composite1.getSize().y+1);
    	composite1.setSize(composite1.getSize().x-1,composite1.getSize().y-1);
    	System.out.println("XCoordinate = " + c.getXCoordinate());
    	System.out.println("YCoordinate = " + c.getYCoordinate());
    }

	public Graph drawing(Composite composite, StyledText styledText2, Table table) throws JMSException, IOException {
		// TODO Auto-generated method stub
		graph=new Graph(composite,SWT.NONE);
		this.styledText=styledText2;
		this.table=table;
		receiver.receive(node_info, graph,styledText,table);
		graph.setConnectionStyle(ZestStyles.CONNECTIONS_DIRECTED);
		graph.addMouseListener(new MouseListener() {
			@Override
			public void mouseDoubleClick(MouseEvent arg0) {
				// TODO Auto-generated method stub
				java.lang.Object object = graph.getSelection();
				try{
                	GraphNode node=(GraphNode) graph.getSelection().get(0);
                	System.out.println(node.getText());
                	for(int i=0;i<node_info.size();i++){
                		if(node_info.get(i).gnode.equals(node)){
                			System.out.println(node_info.get(i).activity.getId());
                			styledText.append(node_info.get(i).activity.getId()+"\n");
                		}
                	}
                }catch(ClassCastException e1){
                	System.out.println("bcc");
                }
                
			}

			@Override
			public void mouseDown(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void mouseUp(MouseEvent arg0) {
				// TODO Auto-generated method stub
				
			}		
		});
		composite1=composite;
		return graph;
	}
}