import ict.csiro.au.Event;

import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.StyledText;
import org.eclipse.swt.widgets.Display;
import org.eclipse.swt.widgets.Table;
import org.eclipse.swt.widgets.TableItem;
import org.eclipse.zest.core.widgets.Graph;
import org.eclipse.zest.core.widgets.GraphConnection;
import org.eclipse.zest.core.widgets.GraphNode;

public class FillTable implements Runnable{
	Table table;
	Event e=null;
	String StartNode=null;
	public void setElements(Table table,Event e,String StartNode){
		this.table=table;
		this.e=e;
		this.StartNode=StartNode;
		System.out.println("in thread+ "+StartNode);
	}
	@Override
	public void run() {
		// TODO Auto-generated method stub
		Display.getDefault().asyncExec(new Runnable(){
			@Override
			public void run() {
				// TODO Auto-generated method stub
				System.out.println();
				TableItem item = new TableItem(table, SWT.NONE);
				int c = 0;
				item.setText(c++, e.getUSERID());
				item.setText(c++, e.getLEVEL());
				item.setText(c++, e.getDATE());
				item.setText(c++, e.getLOGGER());
				table.setTopIndex(table.getItemCount()-1);					
			}
			
		});
	}
}